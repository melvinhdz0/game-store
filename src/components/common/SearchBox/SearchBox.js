import React from 'react';
import './SearchBox.css'

const searchBox = () => {
    return (
        <div>
            <i className="fas fa-search"><input className="searchBar" type = "text"  /></i>  
        </div>
    );
}

export default searchBox;