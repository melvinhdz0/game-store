import React from "react";
import Labels from "../Label/Label";
import './Footer.css'

const footer = (props) => {
  return (
    <div className="footer" >
      <Labels title="Developed in React"/>
      <Labels text="By Melvin Hernandez"/>
    </div>
  );
};

export default footer;