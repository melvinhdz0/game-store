import React from "react";
import Labels from "../Label/Label";
import Banner from "../BannerMain/Banner";
import './Cards.css';

const cards = (props) => {
  let urlHard;
  if(props.img == null){
    urlHard = "https://image.api.playstation.com/vulcan/ap/rnd/202009/2814/GGyEnCkIXoyiVlN9sRHkzUPo.png"
  }else{
    urlHard = `${props.img.url}`
  }
  

  return(
    <div className="bannerCard">
      <Banner urlImg={urlHard}/>
      <Labels title={props.title}/>
      <Labels money={props.price}/>
    </div>
  );
};

export default cards;