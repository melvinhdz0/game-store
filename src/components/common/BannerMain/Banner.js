import React from 'react';
import './Banner.css';

const banner = (props) =>{
  return(
    <img className="banner" src ={props.urlImg} />
  );
} 

export default banner;