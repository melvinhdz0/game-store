import React from "react";

const labels = (props) => {

  let money ='';

  if (props.money){
    money = `$${props.money}`;
  }
  return (
    <div>
      <label>
        <strong>{props.title}</strong>
      </label>
      <label>{props.text}</label>
      <label>{money}</label>
    </div>
  );
};

export default labels;
