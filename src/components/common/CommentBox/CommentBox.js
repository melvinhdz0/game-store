import React from 'react';
import Comments from '../Comments/Comments';
import './CommentBox.css';

const commentBox = ()=>{
  return(
    <div className="commentBox">
      <Comments />
      <Comments />
      <Comments />
      <Comments />
      <Comments />
      <Comments />
    </div>
  );
}

export default commentBox;