import React from "react";
import MenuItems from "../MenuItems/MenuItems";
import SearchBox from "../SearchBox/SearchBox";
import "./NavBar.css";

const navBar = (props) => {
  function displayMenu() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

  const navegacion1 = () => {
      props.navegation({
        indexHtml: 0
      })
    }

  const navegacion2 = () => {
    props.navegation({
      indexHtml: 1
    })
  }

  return (
    <div className="topnav" id="myTopnav">
      <a>
        <MenuItems text="Home" navegation={navegacion1} />
      </a>
      <a>
        <MenuItems text="Game List" navegation={navegacion2} />
      </a>
      <a href="javascript:void(0);" className="icon" onClick={displayMenu}>
      <i className="fas fa-bars"></i>
      </a>
    </div>
  );
};

export default navBar;
