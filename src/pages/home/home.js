import React, { useState, useEffect, useRef } from "react";
import "./home.css";
import NavBar from "../../components/common/NavBar/NavBar";
import Banner from "../../components/common/BannerMain/Banner";
import Cards from "../../components/common/Cards/Cards";
import Footer from "../../components/common/Footer/Footer";

const HomePage = (props) => {
  let cardDisplay = useRef('');

  const [gameState, setGames] = useState(
    [{
    id:'',
    name: '',
    price:  '',
    cover_art: null
    
    }
  ] 
  );

  useEffect(() => {
    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(
      "https://trainee-gamerbox.herokuapp.com/games?_limit=8",
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        setGames(result);
      })
      .catch((error) => console.log("error", error));
  }, []);

  useEffect(() => {
    cardDisplay.current = (
      <div className="body">
        {gameState.map((element) => {
          return(<Cards
            key={element.id}
            title={element.name}
            price={element.price}
            img={element.cover_art}
          ></Cards>
          )
          
        })}
      </div>
    );
    return cardDisplay = ('');
  }, [gameState]);

  return (
    <div className="pageContent">
      <NavBar navegation={props.navegation} />
      <div className="bannerContent">
        <Banner urlImg="https://previews.123rf.com/images/redlinevector/redlinevector1903/redlinevector190300644/119546618-video-games-neon-text-and-gamepad-with-lightnings-video-game-and-entertainment-design-night-bright-n.jpg" />
      </div>
      {cardDisplay.current}
      <Footer />
    </div>
  );
};

export default HomePage;
