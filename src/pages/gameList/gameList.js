import React, {useRef, useEffect, useState} from "react";
import "./gameList.css";
import NavBar from "../../components/common/NavBar/NavBar";
import Cards from "../../components/common/Cards/Cards";
import Footer from "../../components/common/Footer/Footer";
import ArrowButton from "../../components/common/ArrowButton/ArrowButton"

const GamesList = (props) => {
  let cardsDisplay = null;
  let paginatedItems;
  let page = useRef(1);
  let pageItems = 10;

  const [gameListState, setGamesList] = useState(
    [{
    id:'',
    name: '',
    price:  '',
    cover_art: null
    
    }
  ] 
  );

  useEffect(() => {
    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(
      "https://trainee-gamerbox.herokuapp.com/games?_limit=8",
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        setGamesList(result);
      })
      .catch((error) => console.log("error", error));
  }, []);

  useEffect(() => {
    
  }, [gameListState]);

  function pageItemsDisplay(items, itemsPerPage, pageCount) {
    pageCount = pageCount - 1;
    let start = itemsPerPage * pageCount;
    let end = start + itemsPerPage;
    let pageItems = items.slice(start, end);
    return pageItems;
  }

  paginatedItems = pageItemsDisplay(gameListState, pageItems, page.current);
  console.log(paginatedItems);

  function movePageNext(page){
    let newPage = page + 1;
    paginatedItems = pageItemsDisplay(gameListState, pageItems, newPage);
    props.navegation({
      indexHtml: 1,
      pagination: 0
    });
  }
  function movePageBack(page){
    let newPage = page - 1;
    paginatedItems = pageItemsDisplay(gameListState, pageItems, newPage);
    props.navegation({
      indexHtml: 1,
      pagination: 0
    });
  }

  cardsDisplay = (
    <div className="body-list">
      <div className="left-side"></div>
      <div className="middle-side">
        {paginatedItems.map(valor => {
          return <Cards title = {valor.name} price = {valor.price} img = {valor.cover_art} />
        })}
        <div className="navigation-buttons">
          <a onClick={movePageNext.bind(this, page.current)}><ArrowButton /></a>
          <a onClick={movePageBack.bind(this, page.current)}><ArrowButton /></a>
        </div>
      </div>
      <div className="right-side"></div>
  </div>);

  

  return (
    <div className="pageContent">
      <NavBar navegation={props.navegation} />
      {cardsDisplay}

      <Footer />
    </div>
  );
};

export default GamesList;
